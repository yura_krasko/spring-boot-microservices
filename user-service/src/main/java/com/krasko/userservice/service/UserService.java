package com.krasko.userservice.service;

import com.krasko.userservice.dto.UserDto;
import com.krasko.userservice.entity.UserEntity;

import java.util.List;

public interface UserService {

    List<UserDto> getUsers();

    UserDto getUserById(Long userId);

    UserEntity createUser(UserDto userDto);

    UserDto updateUser(Long userId, UserDto userDto);

    void deleteUser(Long userId);
}
