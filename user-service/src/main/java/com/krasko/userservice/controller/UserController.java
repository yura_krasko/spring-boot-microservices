package com.krasko.userservice.controller;

import com.krasko.userservice.dto.UserDto;
import com.krasko.userservice.dto.UserRequestDto;
import com.krasko.userservice.entity.UserEntity;
import com.krasko.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers(){
        return ok(userService.getUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("id") Long id){
        return ok(userService.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<UserEntity> createUser(@RequestBody UserRequestDto userRequest){
        UserDto map = modelMapper.map(userRequest, UserDto.class);
        return ok(userService.createUser(map));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUserInfo(@PathVariable("id") Long id, @RequestBody UserRequestDto userRequest){
        UserDto map = modelMapper.map(userRequest, UserDto.class);
        return ok(userService.updateUser(id, map));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
        return ok().build();
    }
}
